﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace profile
{
    public partial class upDateUser : Form
    {
        database mydb;
        public upDateUser()
        {
            InitializeComponent();

            mydb = new database();
        }

        private void upDateUser_Load(object sender, EventArgs e)
        {
            mydb.openconnection();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 isForm1 = new Form1();
            isForm1.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBoxUpdateId.Text);
            string firstname = textBoxUpdateFirstName.Text;
            string lastname = textBoxUpdateLastName.Text;
            string password = textBoxUpdatePassword.Text;
            string email = textBoxUpdateEmail.Text;


            mydb.upDateUser(id, firstname, lastname, password, email);
        }
    }
}
