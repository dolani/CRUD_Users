﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace profile
{
    public partial class viewUsers : Form
    {
        database mydb;
        MySqlConnection myconnect;
        MySqlCommand mycommand;
        DataTable dTable;
        string con;
        public viewUsers()
        {
            InitializeComponent();
            mydb = new database();
            con = "SERVER=127.0.0.1;username=root;password=password;port=1234;database=admin";
            myconnect = new MySqlConnection(con);
            mycommand = new MySqlCommand();
            dTable = new DataTable();
        }

        
        private void viewUsers_Load(object sender, EventArgs e)
        {
            mydb.openconnection();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 isForm1 = new Form1();
            isForm1.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            myconnect.Open();
            try
            {
                string query = "select * from users";
                mycommand.CommandText = query;
                mycommand.Connection = myconnect;
                mycommand.ExecuteNonQuery();
                MySqlDataAdapter myAdapter = new MySqlDataAdapter();

                myAdapter.SelectCommand = mycommand;
                myAdapter.Fill(dTable);
                dataGridView.DataSource = dTable;
            }
            catch (MySqlException er)
            {
                MessageBox.Show(er.Message);
            }
            finally
            {
                myconnect.Close();
            }
        }   
    }
}
