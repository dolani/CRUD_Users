﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace profile
{
    public partial class newUser : Form
    {
        database mydb;
        public newUser()
        {
            InitializeComponent();

            mydb = new database();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Form1 isForm1 = new Form1();
            isForm1.Show();
            this.Hide();
        }

        private void newUser_Load(object sender, EventArgs e)
        {
            mydb.openconnection();
        }

        private void btnCreateNewUser_Click(object sender, EventArgs e)
        {
            string firstname = textBoxFirstName.Text;
            string lastname = textBoxLastName.Text;
            string password = textBoxPassword.Text;
            string email = textBoxEmail.Text;

            
            mydb.insertNewUser(firstname, lastname, password, email);
        }
        }
    
}
