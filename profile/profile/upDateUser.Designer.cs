﻿namespace profile
{
    partial class upDateUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelUpdateId = new System.Windows.Forms.Label();
            this.labelUpdateFirstName = new System.Windows.Forms.Label();
            this.labelUpdateLastName = new System.Windows.Forms.Label();
            this.labelUpdatePassword = new System.Windows.Forms.Label();
            this.labelUpdateEmail = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxUpdateId = new System.Windows.Forms.TextBox();
            this.textBoxUpdateFirstName = new System.Windows.Forms.TextBox();
            this.textBoxUpdateLastName = new System.Windows.Forms.TextBox();
            this.textBoxUpdatePassword = new System.Windows.Forms.TextBox();
            this.textBoxUpdateEmail = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelUpdateId
            // 
            this.labelUpdateId.AutoSize = true;
            this.labelUpdateId.Location = new System.Drawing.Point(13, 4);
            this.labelUpdateId.Name = "labelUpdateId";
            this.labelUpdateId.Size = new System.Drawing.Size(72, 13);
            this.labelUpdateId.TabIndex = 0;
            this.labelUpdateId.Text = "Insert Your ID";
            // 
            // labelUpdateFirstName
            // 
            this.labelUpdateFirstName.AutoSize = true;
            this.labelUpdateFirstName.Location = new System.Drawing.Point(13, 36);
            this.labelUpdateFirstName.Name = "labelUpdateFirstName";
            this.labelUpdateFirstName.Size = new System.Drawing.Size(35, 13);
            this.labelUpdateFirstName.TabIndex = 1;
            this.labelUpdateFirstName.Text = "label2";
            // 
            // labelUpdateLastName
            // 
            this.labelUpdateLastName.AutoSize = true;
            this.labelUpdateLastName.Location = new System.Drawing.Point(13, 73);
            this.labelUpdateLastName.Name = "labelUpdateLastName";
            this.labelUpdateLastName.Size = new System.Drawing.Size(58, 13);
            this.labelUpdateLastName.TabIndex = 2;
            this.labelUpdateLastName.Text = "Last Name";
            // 
            // labelUpdatePassword
            // 
            this.labelUpdatePassword.AutoSize = true;
            this.labelUpdatePassword.Location = new System.Drawing.Point(13, 107);
            this.labelUpdatePassword.Name = "labelUpdatePassword";
            this.labelUpdatePassword.Size = new System.Drawing.Size(53, 13);
            this.labelUpdatePassword.TabIndex = 3;
            this.labelUpdatePassword.Text = "Password";
            // 
            // labelUpdateEmail
            // 
            this.labelUpdateEmail.AutoSize = true;
            this.labelUpdateEmail.Location = new System.Drawing.Point(16, 145);
            this.labelUpdateEmail.Name = "labelUpdateEmail";
            this.labelUpdateEmail.Size = new System.Drawing.Size(35, 13);
            this.labelUpdateEmail.TabIndex = 4;
            this.labelUpdateEmail.Text = "E-mail";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(52, 192);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Update";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(200, 192);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Home";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxUpdateId
            // 
            this.textBoxUpdateId.Location = new System.Drawing.Point(142, 4);
            this.textBoxUpdateId.Name = "textBoxUpdateId";
            this.textBoxUpdateId.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateId.TabIndex = 7;
            // 
            // textBoxUpdateFirstName
            // 
            this.textBoxUpdateFirstName.Location = new System.Drawing.Point(129, 36);
            this.textBoxUpdateFirstName.Name = "textBoxUpdateFirstName";
            this.textBoxUpdateFirstName.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateFirstName.TabIndex = 8;
            // 
            // textBoxUpdateLastName
            // 
            this.textBoxUpdateLastName.Location = new System.Drawing.Point(129, 65);
            this.textBoxUpdateLastName.Name = "textBoxUpdateLastName";
            this.textBoxUpdateLastName.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateLastName.TabIndex = 9;
            // 
            // textBoxUpdatePassword
            // 
            this.textBoxUpdatePassword.Location = new System.Drawing.Point(129, 99);
            this.textBoxUpdatePassword.Name = "textBoxUpdatePassword";
            this.textBoxUpdatePassword.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdatePassword.TabIndex = 10;
            // 
            // textBoxUpdateEmail
            // 
            this.textBoxUpdateEmail.Location = new System.Drawing.Point(129, 137);
            this.textBoxUpdateEmail.Name = "textBoxUpdateEmail";
            this.textBoxUpdateEmail.Size = new System.Drawing.Size(100, 20);
            this.textBoxUpdateEmail.TabIndex = 11;
            // 
            // upDateUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 308);
            this.Controls.Add(this.textBoxUpdateEmail);
            this.Controls.Add(this.textBoxUpdatePassword);
            this.Controls.Add(this.textBoxUpdateLastName);
            this.Controls.Add(this.textBoxUpdateFirstName);
            this.Controls.Add(this.textBoxUpdateId);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelUpdateEmail);
            this.Controls.Add(this.labelUpdatePassword);
            this.Controls.Add(this.labelUpdateLastName);
            this.Controls.Add(this.labelUpdateFirstName);
            this.Controls.Add(this.labelUpdateId);
            this.Name = "upDateUser";
            this.Text = "upDateUser";
            this.Load += new System.EventHandler(this.upDateUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelUpdateId;
        private System.Windows.Forms.Label labelUpdateFirstName;
        private System.Windows.Forms.Label labelUpdateLastName;
        private System.Windows.Forms.Label labelUpdatePassword;
        private System.Windows.Forms.Label labelUpdateEmail;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxUpdateId;
        private System.Windows.Forms.TextBox textBoxUpdateFirstName;
        private System.Windows.Forms.TextBox textBoxUpdateLastName;
        private System.Windows.Forms.TextBox textBoxUpdatePassword;
        private System.Windows.Forms.TextBox textBoxUpdateEmail;
    }
}