﻿namespace profile
{
    partial class deleteUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteUser = new System.Windows.Forms.Button();
            this.textBoxDeleteUserId = new System.Windows.Forms.TextBox();
            this.labelDeleteUserId = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDeleteUser
            // 
            this.btnDeleteUser.Location = new System.Drawing.Point(38, 150);
            this.btnDeleteUser.Name = "btnDeleteUser";
            this.btnDeleteUser.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteUser.TabIndex = 0;
            this.btnDeleteUser.Text = "Delete";
            this.btnDeleteUser.UseVisualStyleBackColor = true;
            this.btnDeleteUser.Click += new System.EventHandler(this.btnDeleteUser_Click);
            // 
            // textBoxDeleteUserId
            // 
            this.textBoxDeleteUserId.Location = new System.Drawing.Point(96, 60);
            this.textBoxDeleteUserId.Name = "textBoxDeleteUserId";
            this.textBoxDeleteUserId.Size = new System.Drawing.Size(100, 20);
            this.textBoxDeleteUserId.TabIndex = 1;
            this.textBoxDeleteUserId.TextChanged += new System.EventHandler(this.textBoxDeleteUserId_TextChanged);
            // 
            // labelDeleteUserId
            // 
            this.labelDeleteUserId.AutoSize = true;
            this.labelDeleteUserId.Location = new System.Drawing.Point(13, 63);
            this.labelDeleteUserId.Name = "labelDeleteUserId";
            this.labelDeleteUserId.Size = new System.Drawing.Size(71, 13);
            this.labelDeleteUserId.TabIndex = 2;
            this.labelDeleteUserId.Text = "Enter User ID";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(168, 149);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Home";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // deleteUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.labelDeleteUserId);
            this.Controls.Add(this.textBoxDeleteUserId);
            this.Controls.Add(this.btnDeleteUser);
            this.Name = "deleteUser";
            this.Text = "deleteUser";
            this.Load += new System.EventHandler(this.deleteUser_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDeleteUser;
        private System.Windows.Forms.TextBox textBoxDeleteUserId;
        private System.Windows.Forms.Label labelDeleteUserId;
        private System.Windows.Forms.Button button1;
    }
}