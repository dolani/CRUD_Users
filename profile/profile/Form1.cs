﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace profile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void createNewUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newUser isNewUser = new newUser();
            this.Hide();
            isNewUser.Show();

        }

        private void viewUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            viewUsers isViewUsers = new viewUsers();
            this.Hide();
            isViewUsers.Show();
        }

        private void updateUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            upDateUser isUpdateUsers = new upDateUser();
            this.Hide();
            isUpdateUsers.Show();
        }

        private void deleteUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            deleteUser isDeleteUser = new deleteUser();
            this.Hide();
            isDeleteUser.Show();
        }





    }
}
